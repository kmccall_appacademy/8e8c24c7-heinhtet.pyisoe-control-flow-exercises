# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
str.delete!("abcdefghijklmnopqrstuvwxyz")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  return str.length.odd? ? str[mid] : str[mid -1] + str[mid]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.chars.each {|char| count += 1 if VOWELS.include?(char)}
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  multchamber = []
  (1..num).each {|i| multchamber << i }
  multchamber.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each_with_index do |el, idx|
    if idx == arr.length-1
      result += el
    else
      result += el + "#{separator}"
    end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = []
    str.chars.each_with_index do |char, idx|
      if idx.odd?
      result << char.upcase
      else
      result << char.downcase
      end
    end

    result.join()
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
      words = str.split(" ")
      words.each do |word|
        if word.length >= 5
          result << word.reverse
        else
          result << word
        end
      end
      result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
    numbers = (1..n).to_a

    numbers.each_with_index do |num|
      if num % 3 == 0 && num % 5 == 0
      result << "fizzbuzz"
      elsif num % 3 == 0
      result << "fizz"
      elsif num % 5 == 0
      result << "buzz"
      else
        result << num
      end
    end

    result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
    arr.each {|el| reversed.unshift(el) }
    reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  (2..num/2).each {|divisor| return false if num % divisor == 0}
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_list = []
    (1..num).each {
      |potentialfactor| factors_list << potentialfactor if num % potentialfactor == 0
    }
  factors_list
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
pfactors_list = factors(num).select {|factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
pfactor_count = prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = arr.select {|el| el.odd?}
  evens = arr.select {|el| el.even?}
    if odds.count > evens.count
      return evens[0].to_i
    else
      return odds[0].to_i
    end
end
